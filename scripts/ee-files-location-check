#!/usr/bin/env ruby

require_relative 'ee_specific_check/ee_specific_check'

include EESpecificCheck # rubocop:disable Style/MixinUsage
git_version

base = find_compare_base

new_files_in_this_branch_not_at_the_ee_top_level =
  run_git_command("diff #{base.ee_fetch_base}...HEAD --name-status --diff-filter=A -- ./ ':!ee' | cut -f2").lines.map(&:strip)

ee_specific_files_in_ce_master_not_at_the_ee_top_level =
  run_git_command("diff #{base.ce_updated_base}..HEAD --name-status --diff-filter=A -- ./ ':!ee' | cut -f2").lines.map(&:strip)

new_ee_specific_files_not_at_the_ee_top_level =
  new_files_in_this_branch_not_at_the_ee_top_level & ee_specific_files_in_ce_master_not_at_the_ee_top_level

status = 0

new_ee_specific_files_not_at_the_ee_top_level.each do |file|
  next if WHITELIST.any? { |pattern| Dir.glob(pattern).include?(file) }

  puts
  puts "* 💥 #{file} is EE-specific and should be moved to ee/#{file}: 💥"
  puts "  => git mv #{file} ee/#{file}"
  status = 1
end

if status.zero?
  say "🎉 All good, congrats! 🎉"
end

remove_remotes

say "ℹ️ For more information on the why and how of this job, see https://docs.gitlab.com/ee/development/ee_features.html#detection-of-ee-only-files"

exit(status)
